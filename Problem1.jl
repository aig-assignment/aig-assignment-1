### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ 3cd4cd10-91fb-11eb-31ba-2182a0fcdd5c
using Pkg

# ╔═╡ 641049e0-91fb-11eb-097b-d14d1852f704
using PlutoUI

# ╔═╡ 41dfd8e0-91fb-11eb-12b8-f7d8ee6f7cf6
Pkg.activate("Project.tmonl")

# ╔═╡ a825cbd0-b3f9-11eb-2bec-dd2bb17495a2
mutable struct State
	OfficeName::String
	Position::Int64
	Dirt::Vector{Bool}
end

# ╔═╡ dbfa88b0-b3f9-11eb-3af3-75ba3d4fb53a
struct Action
	AName::String
	Cost::Int64
end

# ╔═╡ 1e59a742-b3fa-11eb-1195-a9422af51f4b
begin
		W   = State("W", 1, [true, false, false])
		C1  = State("C1", 2, [true, true, true])
		C2 	= State("C2", 3, [false, true, true])
		E 	= State("E", 4, [false, true, false])
		Co = Action("Collect", 5)
		Me = Action("MoveEast", 3)
		Mw = Action("MoveWest", 3)
		Re = Action("Remain", 1)
end

# ╔═╡ c62cfa20-91fa-11eb-0400-47f05633ced1
Transition_Mod = Dict()

# ╔═╡ b3e045d0-b413-11eb-2c56-2f4872bbc236
begin
	push!(Transition_Mod, C1 => [(Co, C1),(Re, C1),(Mw, W),(Me, C2)])
	push!(Transition_Mod, W => [(Co, W),(Me, C1)])
	push!(Transition_Mod, C2 => [(Co, C2),(Re, C2),(Me, E)])
	push!(Transition_Mod, E => [(Co, E)])
end

# ╔═╡ b62f2cf0-b415-11eb-2c79-13a6563a023b
function goal_test(current_state::State)
	return ! (current_state.Dirt[false, false, false] || current_state.Dirt[false, false, false] || current_state.Dirt[false, false, false] || current_state.Dirt[false, false, false])
end


# ╔═╡ 197fd3e0-b416-11eb-3fcd-591ac766992b



# ╔═╡ Cell order:
# ╠═3cd4cd10-91fb-11eb-31ba-2182a0fcdd5c
# ╠═41dfd8e0-91fb-11eb-12b8-f7d8ee6f7cf6
# ╠═641049e0-91fb-11eb-097b-d14d1852f704
# ╠═a825cbd0-b3f9-11eb-2bec-dd2bb17495a2
# ╠═dbfa88b0-b3f9-11eb-3af3-75ba3d4fb53a
# ╠═1e59a742-b3fa-11eb-1195-a9422af51f4b
# ╠═c62cfa20-91fa-11eb-0400-47f05633ced1
# ╠═b3e045d0-b413-11eb-2c56-2f4872bbc236
# ╠═b62f2cf0-b415-11eb-2c79-13a6563a023b
# ╠═197fd3e0-b416-11eb-3fcd-591ac766992b
