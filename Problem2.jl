### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ 91b1dd30-af1c-11eb-0eaf-f525b8462c13
using Pkg

# ╔═╡ e411a9c0-af1c-11eb-3fc7-8bda2a636a76
using PlutoUI

# ╔═╡ d96f7ec0-af1c-11eb-19d6-0b3940b7d978
Pkg.activate("Project.tmonl") 

# ╔═╡ 5c6e9800-af23-11eb-0f5a-43d5d4b86536
begin
	max = 1000
	min = -1000
end

# ╔═╡ c3a39db0-af26-11eb-212b-2f095a2fba0c
function minimax(depth,index,maxPlayer,alpha,beta,game)
	
    if depth == 1
        return game[index]
    end
	
    if maxPlayer
        best = min
        for i in 1:2
            val = minimax(depth - 1,i,false,alpha,beta,game)
            best = best >= val ? best : val
            alpha = alpha >= val ? alpha : val
            if beta <= alpha
                break
            end
        end
        return best
    else
        best = max
        for i in 1:2
            val = minimax(depth - 1,i,true,alpha,beta,game)
            best = best <= val ? best : val
            beta = beta <= val ? beta : val
            if beta <= alpha
                break
            end
        end
        return best
    end
end

# ╔═╡ 0876a7d0-af3a-11eb-0cde-9547a5a211b3
game = [1, 2, 3, 4, 5, 6, 7, 8]

# ╔═╡ d568aa70-b428-11eb-0a5f-1defcdc34612
minimax(4,1,true,min,max,game)

# ╔═╡ Cell order:
# ╠═91b1dd30-af1c-11eb-0eaf-f525b8462c13
# ╠═d96f7ec0-af1c-11eb-19d6-0b3940b7d978
# ╠═e411a9c0-af1c-11eb-3fc7-8bda2a636a76
# ╠═5c6e9800-af23-11eb-0f5a-43d5d4b86536
# ╠═c3a39db0-af26-11eb-212b-2f095a2fba0c
# ╠═0876a7d0-af3a-11eb-0cde-9547a5a211b3
# ╠═d568aa70-b428-11eb-0a5f-1defcdc34612
